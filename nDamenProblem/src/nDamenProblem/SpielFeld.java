package nDamenProblem;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class SpielFeld implements Comparable<SpielFeld>{

	
	int gen;
	int size;
	int mutPro;
	Field[][] field;
	Queen[] queens;
	
	public SpielFeld(int size, int mutPro){
		this.size = size;
		this.mutPro = mutPro;
		
		field = new Field[size][size];
		queens = new Queen[size];
		//field = new Field[4][4];
		fillEmpty();
		fillQueen();
		//print();
		
		
	}
	
	
	public void solve(){
		
		while(getCollisions() > 0){
			gen++;
			for(int i = 0; i<size;i++){
				queens[i].colls = getCollisions(queens[i]);
			}
			Arrays.sort(queens);
			
			mutate(5);
			
			
			print();
			System.out.println();
			
			try {
				//Thread.sleep(1000);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public Field[][] copyThis(){
		Field[][] f = new Field[size][size];
		for(int i = 0; i <field.length ; i++){
			for(int i1 = 0; i1 <field.length ; i1++){
				f[i][i1] = field[i][i1];
			}
		}
		
		return f;
	}
	
	public void mutate(int chance){
		int r = 0;
		SpielFeld old = new SpielFeld(size, mutPro);
		old.field = copyThis();
		
		
		for(int i = 0; i<size;i++){
			
			if((Math.random()*100) < chance){
				
				field[queens[i].yPos][queens[i].xPos] = new Empty(queens[i].yPos,queens[i].xPos);
				
				
				r = (int)(Math.random() * this.size);
				if(Math.random() > 0.5){
					if((queens[i].yPos + r) > this.size-1){
						queens[i].yPos -=r;
						if(queens[i].yPos < 0){
							queens[i].yPos = 0;
						}
					}else{
						queens[i].yPos +=r;
						if(queens[i].yPos > this.size-1){
							queens[i].yPos = this.size-1;
						}
					}
				}else{
					if((queens[i].yPos - r) < 0){
						queens[i].yPos +=r;
						if(queens[i].yPos > this.size-1){
							queens[i].yPos = this.size-1;
						}
					}else{
						queens[i].yPos -=r;
						if(queens[i].yPos < 0){
							queens[i].yPos = 0;
						}
					}
				}
				
				field[queens[i].yPos][queens[i].xPos] = queens[i];
				
			}
		}
		
		
	}
	
	
	public void fillEmpty(){
		for(int i = 0; i<size;i++){
			for(int i1 = 0; i1<size;i1++){
				field[i][i1] = new Empty(i,i1);
			}
		}
	}
	public void fillQueen(){
		for(int i = 0; i<size;i++){
			int r = (int) (Math.random()*size);
			
			//System.out.println(r);
			Queen q = new Queen(i,r); //!!!!!!!!!!!!
			field[r][i] = q;
			queens[i] = q;
		}
	}
	public void print(){
		System.out.println("GENERATION: " + gen);
		
		for(int i = 0; i<size;i++){
			for(int i1 = 0; i1<size;i1++){
				System.out.print(field[i][i1] + " ");
			}
			System.out.println();
		}
		System.out.println("Collisions: " + getCollisions());
	}
	public int getCollisions(){
		int colls = 0;
		for(int i = 0; i<size;i++){
			for(int i1 = 0; i1<size;i1++){
				if((queens[i1].xPos == queens[i].xPos && queens[i1].yPos != queens[i].yPos) ^
						(queens[i1].xPos != queens[i].xPos && queens[i1].yPos == queens[i].yPos) ^
						(queens[i] != queens[i1] && (Math.abs(queens[i].xPos - queens[i1].xPos) == Math.abs(queens[i].yPos - queens[i1].yPos)))){
					colls++;
					
				}
			}
		}
		
		return colls;
	}
	public int getCollisions(Queen q){
		int colls = 0;
		
			for(int i1 = 0; i1<size;i1++){
				if((queens[i1].xPos == q.xPos && queens[i1].yPos != q.yPos) ^
						(queens[i1].xPos != q.xPos && queens[i1].yPos == q.yPos) ^
						(q != queens[i1] && (Math.abs(q.xPos - queens[i1].xPos) == Math.abs(q.yPos - queens[i1].yPos)))){
					colls++;
					
				}
			
		}
		
		return colls;
	}


	@Override
	public int compareTo(SpielFeld o) {
		// TODO Auto-generated method stub
		return Integer.compare(this.getCollisions(), o.getCollisions());
	}
	
}
