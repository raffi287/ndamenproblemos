package nDamenProblem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class General {

	public int generation;
	
	public int population;
	public int mutChance;
	public int fieldSize;
	ArrayList<SpielFeld> fields;
	
	public General(int population, int mutChance, int fieldSize){
		
		generation = 0;
		
		this.population = population;
		this.mutChance = mutChance;
		this.fieldSize = fieldSize;
		fields = new ArrayList<SpielFeld>(population);
		
		genPopulation();
		Collections.sort(fields);
		solve();
		
	}
	
	public void solve(){
		while(fields.get(0).getCollisions() != 0){
			
			
			cross();
			
			
			for(int i = 0; i<population; i++){
				correctField(fields.get(i));
				fields.get(i).mutate(mutChance);
				Arrays.sort(fields.get(i).queens);
			}
			Collections.sort(fields);
			print(fields.get(0));
			
			
			generation++;
		}
		print(fields.get(0));
	}
	
	public void correctField(SpielFeld sf){
		for(int i = 0; i<fieldSize;i++){
			for(int i1 = 0; i1<fieldSize;i1++){
				if(sf.field[i][i1].type == null || sf.field[i][i1].type.equals("Q")){
					sf.field[i][i1] = new Empty(i,i1);
				}
			}
		}
		
		for(int i = 0; i<fieldSize;i++){
			sf.field[sf.queens[i].yPos][sf.queens[i].xPos] = sf.queens[i];
		}
	}
	
	public void print(SpielFeld s){
		System.out.println("GENERATION: " + generation);
		
		for(int i = 0; i<fieldSize;i++){
			for(int i1 = 0; i1<fieldSize;i1++){
				System.out.print(s.field[i][i1] + " ");
			}
			System.out.println();
		}
		System.out.println("Collisions: " + s.getCollisions());
	}
	
	public void cross(){
		for(int i = 0; i<(population/2);i++){
			Queen[] q = fields.get((population/2)+i).queens;
			q = new Queen[fieldSize];
			
			Field[][] f = fields.get((population/2)+i).field;
			f = new Field[fieldSize][fieldSize];
			
			for(int i1 = 0;i1<fieldSize;i1++){
				if(i1 <(fieldSize/2)){
					q[i1] = new Queen(fields.get(i).queens[i1].xPos, fields.get(i).queens[i1].yPos);
				}else{
					q[i1] = new Queen(fields.get((population/2) - (i+1)).queens[i1].xPos, fields.get((population/2) - (i+1)).queens[i1].yPos);
				}
			}
			for(int i1 = 0;i1<fieldSize;i1++){
				f[q[i1].yPos][q[i1].xPos] = q[i1];
			}
			
		}
	}
	
	public void genPopulation(){
		for(int i = 0; i<population;i++){
			fields.add(new SpielFeld(fieldSize, mutChance));
		}
	}
	
}
